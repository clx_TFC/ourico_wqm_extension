from ourico.core.app import App
from ourico.logger   import logger
from ourico.db       import ClientSession

from app.db.models   import Reservoir

import json


class Ourico (App):

    def on_init (self):
        logger.info('@wqm initialized')
        self.db_session = ClientSession()

    def on_close (self):
        logger.info('@wqm going out')

    def on_client_request (self, request):
        logger.info('@wqm received the request {}'.format(request))
        data = json.loads(request.decode())

        if data.get('operation') == None:
            logger.info('@wqm operation = None')
            return '{"status": "failed", "error": "field _operation_ is necessary"}'

        if data['operation'] == 'read' :
            logger.info('@wqm operation = read')
            if data.get('ids') == None:
                return '{"status": "failed", "error": "field _ids_ is necessary"}'
            return read(self.db_session, data['ids'])

        elif data['operation'] == 'write':
            logger.info('@wqm operation = write')
            if data.get('reservoirs') == None:
                return '{"status": "failed", "error": "field _reservoirs_ is necessary"}'
            return write(self.db_session, data['reservoirs'])
        
        else:
            logger.info('@wqm operation = ???')
            return ('{"status": "failed", "error": "unknown operation _%s_"}' % data['operation'])



def read (db_session, ids):
    """
    Read reservoirs from database
    * db_session - database connection
    * ids        - reservoirs to get
    """

    try :

        if ids == 'all':
            reservoirs = db_session.query(Reservoir).all()
        else:
            reservoirs = db_session.query(Reservoir).filter(Reservoir.id.in_(ids)).all()
        result = []
        for reservoir in reservoirs:
            as_json = {
                'id'          : reservoir.id,
                'name'        : reservoir.name,
                'description' : reservoir.description,
                'ilha'        : reservoir.ilha,
                'concelho'    : reservoir.concelho,
                'cidade'      : reservoir.cidade,
                'height'      : reservoir.height,
                'width'       : reservoir.width,
                'length'      : reservoir.length,
                'base64_image': reservoir.base64_image,
                'nr_inputs'   : reservoir.nr_inputs,
                'nr_outputs'  : reservoir.nr_outputs,
            }
            result.append(as_json)

        data = json.dumps(result)
        return '{"status": "ok", "data": ' + data + '}'

    except Exception as e:
        db_session.rollback()
        logger.error('@wqm.read failed to read with exception {}'.format(str(e)))
        return '{"status": "failed", "error": "failed to read data from database"}'


def write (db_session, reservoirs):
    """
    Save a reservoir
    * db_session - database connection
    * reservoirs - data to save
    """

    try:
        
        for reservoir in reservoirs:
            new_res = Reservoir(
                id           = reservoir['id'],
                name         = reservoir['name'],
                description  = reservoir['description'],
                ilha         = reservoir['ilha'],
                concelho     = reservoir['concelho'],
                cidade       = reservoir['cidade'],
                height       = reservoir['height'],
                width        = reservoir['width'],
                length       = reservoir['length'],
                base64_image = reservoir['base64_image'],
                nr_inputs    = reservoir['nr_inputs'],
                nr_outputs   = reservoir['nr_outputs']
            )
            db_session.add(new_res)
            
        db_session.commit()
        
        return '{"status": "ok"}'

    except Exception as e:
        db_session.rollback()
        logger.error('@wqm.write failed to write with exception {}'.format(str(e)))
        return '{"status": "failed", "error": "failed to write data to database"}'